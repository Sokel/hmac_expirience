from cerberus import Validator

# COMPANY LIST
company_list_response_schema = {
    'companies': {
        'type': 'list',
        'required': True,
        'empty': True,
        'schema': {
            'id': {'type': 'integer', 'empty': False, 'required': True},
            'name': {'type': 'string', 'empty': False, 'required': True},
            'balance': {'type': 'float', 'empty': False, 'required': True},
            'pubkey': {'type': 'string', 'empty': False, 'required': True},
            'seckey': {'type': 'string', 'empty': False, 'required': True}
        }
    }
}
company_list_response_validator = Validator(company_list_response_schema, allow_unknown=False)

# COMPANY TRANSACTION LIST
company_transaction_list_response_schema = {
    'transactions': {
        'type': 'list',
        'required': True,
        'empty': True,
        'schema': {
            'company_id': {'type': 'integer', 'empty': False, 'required': True},
            'id': {'type': 'integer', 'empty': False, 'required': True},
            'sum': {'type': 'float', 'empty': False, 'required': True},
            'type': {'type': 'string', 'allowed': ['income', 'consume'], 'empty': False, 'required': True},
            'type_int': {'type': 'integer', 'allowed': [0, 1], 'empty': False, 'required': True},
            'comment': {'type': 'string', 'empty': False, 'required': True},
            'created_date': {'type': 'string', 'empty': False, 'required': True},
        }
    }
}
company_transaction_list_response_validator = Validator(company_transaction_list_response_schema, allow_unknown=False)

# COMPANY TRANSACTION CREATE
company_transaction_create_request_schema = {
    'sum': {'type': 'float', 'empty': False, 'required': True},
    'type': {'type': 'integer', 'allowed': [0, 1], 'empty': False, 'required': True},
    'comment': {'type': 'string', 'empty': False, 'required': True}
}
company_transaction_create_request_validator = Validator(company_transaction_create_request_schema, allow_unknown=False)

company_transaction_create_response_schema = {
    'company_id': {'type': 'integer', 'empty': False, 'required': True},
    'id': {'type': 'integer', 'empty': False, 'required': True},
    'sum': {'type': 'float', 'empty': False, 'required': True},
    'type': {'type': 'string', 'allowed': ['income', 'consume'], 'empty': False, 'required': True},
    'type_int': {'type': 'integer', 'allowed': [0, 1], 'empty': False, 'required': True},
    'comment': {'type': 'string', 'empty': False, 'required': True},
    'created_date': {'type': 'string', 'empty': False, 'required': True},
}
company_transaction_create_response_validator = Validator(company_transaction_create_response_schema,
                                                          allow_unknown=False)
