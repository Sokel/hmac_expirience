from django.conf.urls import url

from . import views as all_ctrl

v1 = [
    url(r'companies/$', all_ctrl.company_list,
        name='v1_companies_list'),
    url(r'companies/(?P<company_id>[0-9a-z-]+)/transactions/$', all_ctrl.company_transaction_list,
        name='v1_companies_transaction_list'),
    url(r'companies/(?P<company_id>[0-9a-z-]+)/transactions/create/$', all_ctrl.company_transaction_create,
        name='v1_companies_transaction_create'),

]
