from cerberus import Validator
from django.http import JsonResponse
from django.urls import reverse_lazy
import json
import hashlib

from api import utils as api_utils
from core import models as core
import hmac

LOGIN_REDIRECT_URL = reverse_lazy('login')

def hmac_required():
    def decorator(func):
        def wrapper(request, *args, **kwargs):
            pub_key = request.META.get('X-Auth-Token', None)
            if not pub_key:
                pub_key = request.META.get('HTTP_X_AUTH_TOKEN', None)
            if not pub_key:
                return api_utils.send_unauthorized('needed auth token not presented')

            signature = request.META.get('X-Auth-Signature', None)
            if not signature:
                signature = request.META.get('HTTP_X_AUTH_SIGNATURE', None)
            if not signature:
                return api_utils.send_unauthorized('needed signature not presented')

            try:
                company = core.Company.objects.get(pub_key=pub_key)
            except:
                return api_utils.send_not_found('company not found')

            if request.method == 'GET':
                hashed_body = request.get_full_path()
                hashed_body = hashed_body.encode()
            elif request.method == 'POST':
                hashed_body = request.body
            else:
                return api_utils.send_not_allowed('method not allowed')

            sec_key = company.secret_key.encode()
            hmac_hash = hmac.new(sec_key, hashed_body, digestmod=hashlib.sha1).hexdigest()
            if hmac_hash != signature:
                return api_utils.send_unauthorized('signature not allowed')
            return func(request, *args, **kwargs)

        return wrapper

    return decorator


def method_allowed(methods=('GET',)):
    def decorator(func):
        def wrapper(request, *args, **kwargs):
            if request.method not in methods:
                return api_utils.send_not_allowed('Method Not Allowed')

            return func(request, *args, **kwargs)

        return wrapper

    return decorator


def schema_required(schema=None):
    def decorator(func):
        def wrapper(request, *args, **kwargs):
            body = json.loads(request.body.decode("utf-8"))
            v = Validator(schema=schema)
            valid = v.validate(body)
            if not valid:
                return JsonResponse(data={'error': v.errors}, status=400)
            return func(request, body, *args, **kwargs)

        return wrapper

    return decorator


def schema_required_free(schema=None):
    def decorator(func):
        def wrapper(request, *args, **kwargs):
            import json
            try:
                body = json.loads(request)
            except json.JSONDecodeError:
                return JsonResponse(data={'error': 'JSON decode error'}, status=400)
            v = Validator(schema=schema)
            valid = v.validate(body)
            if not valid:
                return JsonResponse(data={'error': v.errors}, status=400)
            return func(request, body, *args, **kwargs)

        return wrapper

    return decorator
