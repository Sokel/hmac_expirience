from django.views.decorators.csrf import csrf_exempt

from . import utils as api_utils
from . import schemas as all_schemas
from . import decorators
from core import models as core


@csrf_exempt
@decorators.method_allowed(['GET', ])
def company_list(request):
    companies = core.Company.get_all()

    companies_list = []
    for company in companies:
        companies_list.append(company.to_dict())
    return api_utils.send_json_response({
        'companies': companies_list
    })


@csrf_exempt
@decorators.method_allowed(['GET', ])
@decorators.hmac_required()
def company_transaction_list(request, company_id):
    company = core.Company.get_by_id(company_id)
    if not company:
        return api_utils.send_not_found('company not found')

    transactions_list = []
    for transaction in company.transactions:
        transactions_list.append(transaction.to_dict())

    return api_utils.send_json_response({
        'transactions': transactions_list
    })


@csrf_exempt
@decorators.method_allowed(['POST', ])
@decorators.hmac_required()
@decorators.schema_required(all_schemas.company_transaction_create_request_schema)
def company_transaction_create(request, body, company_id):
    company = core.Company.get_by_id(company_id)

    if not company:
        return api_utils.send_not_found('company not found')

    transaction = core.Transaction()

    transaction.company = company
    transaction.sum = body['sum']
    transaction.type = body['type']
    transaction.comment = body['comment']

    try:
        transaction.save()
        transaction.refresh_from_db()
        transaction_dict = transaction.to_dict()
        return api_utils.send_json_response(transaction_dict)
    except:
        return api_utils.send_bad_request('system error')
