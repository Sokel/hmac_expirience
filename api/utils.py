from django.http import JsonResponse


def send_json_response(data=None, status: int = 200):
    if data is None:
        data = dict()
    return JsonResponse(data, status=status)


def send_bad_request(error: str):
    return JsonResponse(data={'error': error}, status=400)


def send_unauthorized(error: str):
    return JsonResponse(data={'error': error}, status=401)


def send_not_found(error: str):
    return JsonResponse(data={'error': error}, status=404)


def send_not_allowed(error: str):
    return JsonResponse(data={'error': error}, status=405)
