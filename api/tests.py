from django.test import TestCase, Client
from django.urls import reverse

import hashlib
import hmac
import json

from core import models as core
from api import schemas as all_schemas


class BaseTest(TestCase):
    def setUp(self):
        pass

    def is_ok(self, rs):
        self.assertEqual(rs.status_code, 200)

    def is_bad(self, rs):
        self.assertEqual(rs.status_code, 400)

    def is_unauthorized(self, rs):
        self.assertEqual(rs.status_code, 401)

    def is_forbidden(self, rs):
        self.assertEqual(rs.status_code, 403)

    def is_not_allowed(self, rs):
        self.assertEqual(rs.status_code, 405)

    def client_post_json(self, url, data, **kwargs):
        return self.client.post(url, json.dumps(data), content_type='application/json', **kwargs)

    def client_get_json(self, url):
        rs = self.client.get(url)
        return rs


class CompanyTest(BaseTest):
    def client_get_json_with_auth(self, url, data, **kwargs):
        kwargs.update({
            'X-Auth-Token': self.company_first.pub_key,
            'X-Auth-Signature': hmac.new(self.company_first.secret_key.encode(), url.encode(),
                                         digestmod=hashlib.sha1).hexdigest()
        })
        return self.client.get(url, data, content_type='application/json', **kwargs)

    def client_post_json_with_auth(self, url, data, **kwargs):
        body_json = json.dumps(data)
        kwargs.update({
            'X-Auth-Token': self.company_first.pub_key,
            'X-Auth-Signature': hmac.new(self.company_first.secret_key.encode(), body_json.encode(),
                                         digestmod=hashlib.sha1).hexdigest()
        })
        return self.client.post(url, body_json, content_type='application/json', **kwargs)

    def setUp(self):
        self.company_first = core.Company(
            name='FirstCompany'
        )
        self.company_first.save()
        self.company_first = core.Company.get_by_id(self.company_first.pk)
        self.company_second = core.Company(
            name='SecondCompany'
        )
        self.company_second.save()
        self.company_second = core.Company.get_by_id(self.company_second.pk)

        super(BaseTest, self).setUp()

    def test_company_list_test_success(self):
        url = reverse('v1_companies_list')

        response = self.client_get_json(url)
        body = response.json()
        self.is_ok(response)
        result = all_schemas.company_list_response_validator.validate(body)
        self.assertTrue(result)

    def test_company_list_test_error(self):
        url = reverse('v1_companies_list')

        # not allowed
        response = self.client_post_json(url, {})
        self.is_not_allowed(response)

    def test_company_transaction_list_test_success(self):
        url = reverse('v1_companies_transaction_list', kwargs={'company_id': self.company_first.pk})

        response = self.client_get_json_with_auth(url, {})
        body = response.json()
        self.is_ok(response)
        result = all_schemas.company_transaction_list_response_validator.validate(body)
        self.assertTrue(result)

    def test_company_transaction_list_test_error(self):
        url = reverse('v1_companies_transaction_list', kwargs={'company_id': self.company_first.pk})

        # method not allowed
        response = self.client_post_json_with_auth(url, {})
        self.is_not_allowed(response)

        # pubkey not presented
        # signature not presented
        response = self.client_get_json(url)
        self.is_unauthorized(response)

    def test_company_transaction_create_success(self):
        url = reverse('v1_companies_transaction_create', kwargs={'company_id': self.company_first.pk})
        data = {"sum": 100, "type": 0, "comment": "Пополнение счета через QIWI терминал"}

        response = self.client_post_json_with_auth(url, data)
        body = response.json()
        self.is_ok(response)
        result = all_schemas.company_transaction_create_response_validator.validate(body)
        if not result:
            print(body)
            print(all_schemas.company_transaction_create_response_validator.errors)
        self.assertTrue(result)

    def test_company_transaction_create_error(self):
        url = reverse('v1_companies_transaction_create', kwargs={'company_id': self.company_first.pk})
        data = {"sum": 100, "type": 0, "comment": "Пополнение счета через QIWI терминал"}

        # method not allowed
        response = self.client_get_json_with_auth(url, data)
        self.is_not_allowed(response)

        # pubkey not presented
        # signature not presented
        response = self.client_post_json(url, data)
        self.is_unauthorized(response)
