from django.conf import settings
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import QuerySet
from pytz import timezone

import datetime


def get_default_tz():
    tz = timezone(settings.TIME_ZONE)
    return tz


def get_now_with_tz():
    tz = get_default_tz()
    return datetime.datetime.now(tz)
