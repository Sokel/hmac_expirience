from django.contrib import admin

from . import models as core

admin.site.register(core.Company)
admin.site.register(core.Transaction)
