from django.db import models, connection
from . import const as core_const
from . import utils as core_utils
import uuid


class Company(models.Model):
    name = models.CharField(max_length=255, verbose_name='Name')
    pub_key = models.CharField(max_length=255, blank=True, verbose_name='Public Key')
    secret_key = models.CharField(max_length=255, blank=True, verbose_name='Secret Key')
    balance = models.DecimalField(decimal_places=2, max_digits=20, default=0.0, verbose_name='Balance')

    def create_transaction(self, transaction_sum, transaction_type, comment):
        transaction = Transaction(
            company=self,
            sum=transaction_sum,
            type=transaction_type,
            comment=comment,
            created_date=core_utils.get_now_with_tz()
        )
        try:
            transaction.save()
            return transaction
        except:
            return False

    @property
    def transactions(self):
        return Transaction.objects.all().filter(company=self)

    @staticmethod
    def get_by_id(company_id):
        try:
            return Company.objects.get(pk=company_id)
        except models.ObjectDoesNotExist:
            return None
        except:
            return False

    @staticmethod
    def get_all():
        return Company.objects.all()

    def save(self, *args, **kwargs):
        if not self.pub_key:
            self.pub_key = uuid.uuid4()
        if not self.secret_key:
            self.secret_key = uuid.uuid4()
        super(Company, self).save(*args, **kwargs)

    def to_dict(self):
        company_dict = {
            'id': self.pk,
            'name': self.name,
            'balance': float(self.balance),
            'pubkey': self.pub_key,
            'seckey': self.secret_key
        }
        return company_dict

    def __str__(self):
        return str(self.pk) + ' - ' + str(self.name)

    class Meta:
        db_table = 'core_company'
        verbose_name = 'Company'
        verbose_name_plural = 'Companies'


class Transaction(models.Model):
    sum = models.DecimalField(decimal_places=2, max_digits=20, default=0.0, verbose_name='Sum')
    type = models.IntegerField(verbose_name='Type', null=False, blank=False)
    comment = models.TextField(verbose_name='Comment', null=False, blank=True)
    created_date = models.DateTimeField(verbose_name='Created datetime', null=False)
    company = models.ForeignKey(to='Company', verbose_name='Company')

    def save(self, *args, **kwargs):
        try:
            if self.type == core_const.TRANSACTION_TYPE_INCOME:
                transaction_sum = self.sum
            elif self.type == core_const.TRANSACTION_TYPE_CONSUME:
                transaction_sum = -self.sum
            else:
                raise RuntimeError
            with connection.cursor() as cursor:
                cursor.execute("""
                BEGIN;
                SELECT * FROM core_transaction FOR UPDATE;
                """)
                cursor.execute("""
                INSERT INTO core_transaction
                (sum,type,company_id,comment,created_date)
                VALUES (%s, %s, %s, %s, NOW());
                """, [self.sum, self.type, self.company.pk, self.comment])
                cursor.execute("""
                SELECT * FROM core_company WHERE id=%s FOR UPDATE;
                UPDATE core_company SET balance = balance + %s WHERE id = %s;
                END;
                """, [self.company.pk, transaction_sum,
                      self.company.pk])
            transaction = Transaction.objects.earliest('id')
            self.pk = transaction.pk
            return True
        except:
            raise RuntimeError

    def to_dict(self):
        if self.type == core_const.TRANSACTION_TYPE_INCOME:
            transaction_type = core_const.TRANSACTION_TYPE_INCOME_VERB
        elif self.type == core_const.TRANSACTION_TYPE_CONSUME:
            transaction_type = core_const.TRANSACTION_TYPE_CONSUME_VERB
        else:
            transaction_type = 'unrecognized'

        transaction_dict = {
            'company_id': self.company.pk,
            'id': self.pk,
            'sum': float(self.sum),
            'type': transaction_type,
            'type_int': self.type,
            'comment': self.comment,
            'created_date': self.created_date
        }
        return transaction_dict

    def __str__(self):
        return str(self.pk) + ' - ' + str(self.company.name) + ' : ' + self.created_date.__str__()

    class Meta:
        db_table = 'core_transaction'
        verbose_name = 'Transaction'
        verbose_name_plural = 'Transactions'
