default:
	./manage.py runserver

test:
	clear
	python3 -m pytest -v ./**/test*.py

test_cov:
	clear
	python3 -m pytest --cov=./ -v ./**/test*.py

test_cov_html:
	clear
	python3 -m pytest --cov=./ -v ./**/test*.py --cov-report=html

mk_mg:
	python3 manage.py makemigrations

mg:
	python3 manage.py migrate

ssl:
	python3 manage.py runsslserver

shell:
	python3 manage.py shell

nohup:
	nohup python3 manage.py runserver --noreload 0.0.0.0:8083 &
